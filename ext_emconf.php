<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Herrnhuter Losung',
	'description' => 'Contains the current watchword from the Herrnhuter Brüdergemeine / Losungen',
	'category' => 'plugin',
	'author' => 'Philipp Schumann',
	'author_email' => 'ph.schumann@gmx.de',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => true,
	'version' => '12.0.1',
	'constraints' => array(
		'depends' => array(
            'php' => '8.1.0-0.0.0',
            'typo3' => '12.4.0-12.99.99',
            'fluid' => '',
            'extbase' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	)
);
